using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.Runtime
{
    public class MusicBox : MonoBehaviour
    {
        #region Exposed
        
        [SerializeField]
        private GameObject[] _soundBars;
        [SerializeField]
        private Material _off;
        [SerializeField]
        private Material _on;

        #endregion


        #region Unity API
        
        private void Awake()
        {
            InitializeSoundBars();
        }
        
        void OnTriggerEnter2D(Collider2D col)
        {
            if(col.GetComponent<Particle>() != null)
            {
                Debug.Log("YESS");
            }
        }
        void OnTriggerExit2D(Collider2D col)
        {
            if(col.GetComponent<Particle>() != null)
            {
                ;
            }
        }
        #endregion


        #region Main

        private void InitializeSoundBars()
        {
            _audioSource = GetComponent<AudioSource>();
            _audioSource.volume = 0;
            _renderers = new Renderer[_soundBars.Length];

            for (int i = 0; i < _soundBars.Length; i++)
            {
                _renderers[i] = _soundBars[i].GetComponent<Renderer>();
                _renderers[i].material = _off;
            }
        }
        
        private void ModifyVolume(float volume)
        {
            _audioSource.volume = volume;
        }

        #endregion


        #region Private

        private AudioSource _audioSource;
        private Renderer[] _renderers;

        private float _particleCapacity;
        
        #endregion
    }
}
