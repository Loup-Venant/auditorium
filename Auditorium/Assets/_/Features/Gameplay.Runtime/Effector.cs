using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.Runtime
{
    public class Effector : MonoBehaviour
    {
        #region Exposed
        [SerializeField, Range(10f, 50f)]
        private float _boostSpeed;
        [SerializeField, Range(-180f, 180f)]
        private float _angle;

        #endregion


        #region Unity API
        // Start is called before the first frame update
        void Awake()
        {
            _effector = GetComponent<AreaEffector2D>();
            _sprite = GetComponent<SpriteRenderer>().sprite;

            _effector.forceAngle = _angle;
            _effector.forceMagnitude = _boostSpeed;
        
        }
        #endregion

        #region Private

        private AreaEffector2D _effector;
        private Sprite _sprite;

        #endregion
    }
}