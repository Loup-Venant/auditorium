using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.Runtime
{
    public class Particle : MonoBehaviour
    {
        #region Exposed

        [SerializeField]
        private float _deathSpeed;
        [SerializeField]
        private float _startingVelocity;
        [SerializeField]
        private float _slowingFactor;

        #endregion


        #region Unity API

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _rigidbody.velocity = Vector2.right * _startingVelocity;
            _rigidbody.drag = _slowingFactor;
            _speed = _rigidbody.velocity.magnitude;
        }

        private void FixedUpdate()
        {
            
            _speed = _rigidbody.velocity.magnitude;
            if(_speed <= _deathSpeed)
            {
                Destroy(gameObject);
            }
        }

        private void OnGUI()
        {
            GUI.Label(Rect.zero, _speed.ToString());
        }

        #endregion
        
        #region Private

        private Rigidbody2D _rigidbody;
        private float _speed;

        #endregion
    }
}
