using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.Runtime
{
    public class AudioManager : MonoBehaviour
    {
        #region Unity API
        
        private void Awake()
        {
            _audioSources = FindObjectsOfType<AudioSource>();
            foreach (AudioSource source in _audioSources)
            {
                source.Play();
            }
            
        }

        #endregion




        #region Private
        
        private AudioSource[] _audioSources;
        
        #endregion
    }
}
