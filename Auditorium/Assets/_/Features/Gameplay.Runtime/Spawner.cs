using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Gameplay.Runtime
{
    public class Spawner : MonoBehaviour
    {

        #region Exposed

        [SerializeField]
        private float _timeTillNextParticuleInSeconds;
        [SerializeField]
        private float _spawnRadius;
        [SerializeField]
        private GameObject _particule;

        #endregion


        #region Unity API

        private void Awake()
        {
            _spawnPosition = transform.position;
        }
        private void Update()
        {
            _particuleSpawnPosition = Random.insideUnitCircle * _spawnRadius;
            _currentTime += Time.deltaTime;


            if(_currentTime >= _timeTillNextParticuleInSeconds)
            {
                var particule = Instantiate(_particule, _particuleSpawnPosition, Quaternion.identity);
                _currentTime = 0;
            }
            
        }
        #endregion
        

        #region Private

        private Vector2 _spawnPosition;
        private AreaEffector2D _effector;
        private Vector2 _particuleSpawnPosition;
        private float _currentTime;

        #endregion
    }
}